<?php
Namespace dgifford\Traits;

/*
	Method calling trait.

    Copyright (C) 2015  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */




trait MethodCallingTrait
{
	/**
	 * Calls a method within this class if it exists, including parameters.
	 * If it doesn't exist, it returns the $no_exists_return value
	 * 
	 * @param  callable  $callback        	The method to call in $this or an array containing object and method
	 * @param  mixed  $parameters       	Arguments sent to the method
	 * @param  mixed $no_exists_return		Value to return if the method doesn't exist
	 * @return mixed                    	Return value from the method or $no_exists_return
	 */
	public function callIfMethodExists( $callback = '', $parameters = null, $no_exists_return = false)
	{
		// If a string callable is provided, check if it exists within this class
		if( !is_callable( $callback ) and is_string( $callback ) )
		{
			$callback = [ $this, $callback ];
		}

		if( is_callable( $callback ) )
		{
			if( is_array( $parameters ) )
			{
				return call_user_func_array( $callback, $parameters );
			}
			else
			{
				return call_user_func( $callback, $parameters );
			}
		}

		return $no_exists_return;
	}
}