# Method Calling Trait #

Trait for dynamically calling methods within classes.

```
$result = $this->callIfMethodExists( $callback = '', $parameters = null, $no_exists_return = false);
```